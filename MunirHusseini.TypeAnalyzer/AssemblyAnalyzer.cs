﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyAnalyzer.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.CodeDom;

namespace MunirHusseini.TypeAnalyzer
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Analyzes assemblies for type declarations.
    /// </summary>
    internal class AssemblyAnalyzer : MarshalByRefObject, IAssemblyAnalyzer
    {
        private static readonly Dictionary<System.Type, Type> Types = new Dictionary<System.Type, Type>();

        /// <summary>
        /// Gets information about the types defined in the specified project.
        /// </summary>
        /// <param name="path">The path to the assembly to analyze.</param>
        /// <returns>
        /// The information about all types defined in the specified project.
        /// </returns>
        public Assembly Analyze(string path)
        {
            var ass = System.Reflection.Assembly.LoadFrom(path);
            var name = ass.GetName();

            return new Assembly
            {
                Name = name.Name,
                FullName = ass.FullName,
                Version = name.Version.ToString(),
                Path = ass.Location,
                Types = ass.GetTypes().Select(AnalyzeType).ToArray()
            };
        }

        private static readonly System.Type[] SystemTypes =
        {
            typeof (int),
            typeof (uint),
            typeof (short),
            typeof (ushort),
            typeof (long),
            typeof (ulong),
            typeof (byte),
            typeof (string),
            typeof (DateTime),
            typeof (bool),
            typeof (TimeSpan),
            typeof (float),
            typeof (decimal),
            typeof (double),
            typeof (void),
            typeof (char),
            typeof (DateTimeOffset),
            typeof (Guid),
            typeof (IntPtr),
            typeof (UIntPtr),
            typeof (Nullable),
            typeof (Nullable<>),
            typeof (sbyte),

            typeof (int?),
            typeof (short?),
            typeof (uint?),
            typeof (ushort?),
            typeof (long?),
            typeof (ulong?),
            typeof (byte?),
            typeof (DateTime?),
            typeof (bool?),
            typeof (TimeSpan?),
            typeof (float?),
            typeof (decimal?),
            typeof (double?),
            typeof (char?),
            typeof (DateTimeOffset?),
            typeof (Guid?),
            typeof (IntPtr?),
            typeof (UIntPtr?),
            typeof (sbyte?),

            typeof (int[]),
            typeof (uint[]),
            typeof (short[]),
            typeof (ushort[]),
            typeof (long[]),
            typeof (ulong[]),
            typeof (byte[]),
            typeof (string[]),
            typeof (DateTime[]),
            typeof (bool[]),
            typeof (TimeSpan[]),
            typeof (float[]),
            typeof (decimal[]),
            typeof (double[]),
            typeof (char[]),
            typeof (DateTimeOffset[]),
            typeof (Guid[]),
            typeof (IntPtr[]),
            typeof (UIntPtr[]),
            typeof (sbyte[])
        };

        /// <summary>
        /// Analyzes the specified type and its members and attribtues.
        /// </summary>
        /// <param name="type">
        /// The Code DOM type.
        /// </param>
        /// <returns>
        /// The <see cref="Type"/> that describes the specified type.
        /// </returns>
        internal static Type AnalyzeType(System.Type type)
        {
            try
            {
                Type result;

                if (Types.TryGetValue(type, out result))
                {
                    return result;
                }

                result = new Type
                {
                    #region ...

                    Name = type.Name,
                    Namespace = type.Namespace,
                    FullName =
                        string.IsNullOrWhiteSpace(type.FullName) ? type.Namespace + "." + type.Name : type.FullName,
                    AssemblyQualifiedName = type.AssemblyQualifiedName,
                    CodeFullName = GetCodeFullName(type),
                    CodeName = GetCodeName(type),
                    Methods = type.GetMethods().Where(m => !m.IsSpecialName).Select(AnalyzeMethod).ToArray(),
                    Attributes = GetAttributes(type).ToArray(),
                    IsAbstract = type.IsAbstract,
                    IsAnsiClass = type.IsAnsiClass,
                    IsArray = type.IsArray,
                    IsAutoClass = type.IsAutoClass,
                    IsAutoLayout = type.IsAutoLayout,
                    IsByRef = type.IsByRef,
                    IsComObject = type.IsCOMObject,
                    IsClass = type.IsClass,
                    IsConstructedGenericType = type.IsConstructedGenericType,
                    IsContextful = type.IsContextful,
                    IsEnum = type.IsEnum,
                    IsExplicitLayout = type.IsExplicitLayout,
                    IsGenericParameter = type.IsGenericParameter,
                    IsGenericType = type.IsGenericType,
                    IsGenericTypeDefinition = type.IsGenericTypeDefinition,
                    IsImport = type.IsImport,
                    IsInterface = type.IsInterface,
                    IsLayoutSequential = type.IsLayoutSequential,
                    IsMarshalByRef = type.IsMarshalByRef,
                    IsNested = type.IsNested,
                    IsNestedAssembly = type.IsNestedAssembly,
                    IsNestedFamAndAssem = type.IsNestedFamANDAssem,
                    IsNestedFamOrAssem = type.IsNestedFamORAssem,
                    IsNestedFamily = type.IsNestedFamily,
                    IsNestedPrivate = type.IsNestedPrivate,
                    IsNestedPublic = type.IsNestedPublic,
                    IsNotPublic = type.IsNotPublic,
                    IsPointer = type.IsPointer,
                    IsPrimitive = type.IsPrimitive,
                    IsPublic = type.IsPublic,
                    IsSealed = type.IsSealed,
                    IsSecurityCritical = type.IsSecurityCritical,
                    IsSecuritySafeCritical = type.IsSecuritySafeCritical,
                    IsSecurityTransparent = type.IsSecurityTransparent,
                    IsSerializable = type.IsSerializable,
                    IsSpecialName = type.IsSpecialName,
                    IsUnicodeClass = type.IsUnicodeClass,
                    IsValueType = type.IsValueType,
                    IsVisible = type.IsVisible,
                    Guid = type.GUID,
                    IsSystem = type == typeof(object) || SystemTypes.Any(t => t.IsAssignableFrom(type))

                    #endregion ...
                };

                Types.Add(type, result);

                var enumerableInfo = GetEnumerableInfo(type, result);

                result.Properties =
                    type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance |
                                       BindingFlags.Static).Select(p => AnalyzeProperty(p, type)).ToArray();
                result.EnumerableInfo = enumerableInfo;
                result.IsEnumerable = enumerableInfo != null;
                result.BaseType = type.BaseType == null ? null : AnalyzeType(type.BaseType);

                if (result.IsEnum)
                {
                    var names = Enum.GetNames(type);
                    var values = Enum.GetValues(type)
                        .Cast<object>()
                        .Select(o =>
                        {
                            try
                            {
                                var i = (int)o;
                                return (int?)i;
                            }
                            catch
                            {
                                return null;
                            }
                        })
                        .ToList();

                    result.EnumValues = names.Select((n, i) => new EnumValue
                    {
                        Name = n,
                        Value = values[i]
                    }).ToArray();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new AssemblyAnalysisFailedException(type, ex);
            }
        }

        /// <summary>
        /// Analyzes the specified method.
        /// </summary>
        /// <param name="method">
        /// The Code DOM method.
        /// </param>
        /// <returns>
        /// The <see cref="Method"/> that describes the method.
        /// </returns>
        private static Method AnalyzeMethod(MethodInfo method)
        {
            return new Method
            {
                Name = method.Name,
                DeclaringType = GetCodeFullName(method.DeclaringType),
                ReturnType = GetCodeFullName(method.ReturnType),
                Attributes = GetAttributes(method).ToArray(),
                Parameters = method.GetParameters().Select(AnalyzeParameter).ToArray(),
                IsPrivate = method.IsPrivate,
                IsPublic = method.IsPublic,
                IsFamily = method.IsFamily,
                IsFamilyAndAssembly = method.IsFamilyAndAssembly,
                IsFamilyOrAssembly = method.IsFamilyOrAssembly,
                IsAbstract = method.IsAbstract,
                IsStatic = method.IsStatic,
                IsVirtual = method.IsVirtual,
                IsFinal = method.IsFinal
            };
        }

        private static IEnumerable<Attribute> GetAttributes(MethodInfo method)
        {
            return GetAttributes(method.GetCustomAttributes(true));
        }

        private static IEnumerable<Attribute> GetAttributes(System.Type type)
        {
            return GetAttributes(type.GetCustomAttributes(true));
        }

        private static IEnumerable<Attribute> GetAttributes(IEnumerable<object> customAttribtues)
        {
            return from attribute in customAttribtues
                let attributeType = attribute.GetType()
                let properties = attributeType.GetProperties(BindingFlags.Public |
                                                             BindingFlags.Instance |
                                                             BindingFlags.FlattenHierarchy)
                    .Where(p => p.CanRead)
                select new Attribute
                {
                    FullName = attribute.GetType().FullName,
                    Properties = (from p in properties
                        select new AttributeProperty
                        {
                            Name = p.Name,
                            Value = p.GetGetMethod().Invoke(attribute, null).NotNull(v => v.ToString()) as string,
                            Type = p.PropertyType.FullName
                        }).ToArray()
                };
        }

        /// <summary>
        /// Analyzes the specified method parameter.
        /// </summary>
        /// <param name="parameter">
        /// The Code DOM parameter.
        /// </param>
        /// <returns>
        /// The <see cref="Parameter"/> that describes the parameter.
        /// </returns>
        private static Parameter AnalyzeParameter(ParameterInfo parameter)
        {
            return new Parameter
            {
                Name = parameter.Name,
                Type = GetCodeFullName(parameter.ParameterType),
                Attributes = parameter.GetCustomAttributes(true).Select(o => o.GetType().FullName).ToArray()
            };
        }

        private static Property AnalyzeProperty(PropertyInfo p, System.Type type)
        {
            var m = p.GetMethod ?? p.SetMethod;

            return new Property
            {
                Name = p.Name,
                CanRead = p.CanRead,
                CanWrite = p.CanWrite,
                DeclaringType = (p.DeclaringType ?? type).FullName,
                Type = AnalyzeType(p.PropertyType),
                Attributes = p.GetCustomAttributes(true).Select(o => o.GetType().FullName).ToArray(),
                IsVirtual = m.IsVirtual,
                IsPublic = m.IsPublic,
                IsPrivate = m.IsPrivate,
                IsNotPublic = !m.IsPublic,
                IsProtected = m.IsFamily,
                IsInternal = m.IsAssembly,
                IsStatic = m.IsStatic,
                IsAbstract = m.IsAbstract,
            };
        }

        private static EnumerableInfo GetEnumerableInfo(System.Type type, Type defaultValue)
        {
            if (type == null || type.IsGenericTypeDefinition || !typeof(IEnumerable).IsAssignableFrom(type)) return null;

            var enumerableType = defaultValue ?? AnalyzeType(type);
            Type itemType;
            System.Type nestedType;

            if (type.IsGenericType)
            {
                nestedType = type.GetGenericArguments().First();
                itemType = AnalyzeType(nestedType);
            }
            else if (type.HasElementType)
            {
                nestedType = type.GetElementType();
                itemType = AnalyzeType(nestedType);
            }
            else
            {
                nestedType = null;
                itemType = AnalyzeType(typeof(object));
            }

            return new EnumerableInfo
            {
                EnumerableType = enumerableType,
                ItemType = itemType,
                NestedEnumerable = GetEnumerableInfo(nestedType, null)
            };
        }

        public static string GetCodeFullName(System.Type type)
        {
            if (!type.IsGenericType)
            {
                return type.FullName;
            }

            var name = type.FullName ?? type.Namespace + "." + type.Name;
            var index = name.IndexOf("`", StringComparison.Ordinal);

            if (index < 0)
            {
                return name;
            }

            var outerType = name.Substring(0, index);
            var innerTypes = string.Join(", ", type.GetGenericArguments().Select(GetCodeFullName));

            return outerType + "<" + innerTypes + ">";
        }

        public static string GetCodeName(System.Type type)
        {
            if (!type.IsGenericType)
            {
                return type.Name;
            }

            var index = type.Name.IndexOf("`", StringComparison.Ordinal);
            if (index < 0)
            {
                return type.Name;
            }

            var outerType = type.Name.Substring(0, index);
            var innerTypes = string.Join(", ", type.GetGenericArguments().Select(GetCodeName));

            return outerType + "<" + innerTypes + ">";
        }
    }
}