﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProjectAnalyzer.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;

    using global::EnvDTE;

    using EnvDTE80;

    using EnvDTE;

    /// <summary>
    /// Analyzes Visual Studio projects for type declarations.
    /// </summary>
    internal class ProjectAnalyzer
    {
        /// <summary>
        /// A collection of types that are "primitive".
        /// </summary>
        private static readonly string[] PrimitiveTypes =
            {
                typeof(bool).FullName,
                typeof(byte).FullName,
                typeof(sbyte).FullName,
                typeof(short).FullName, 
                typeof(ushort).FullName, 
                typeof(int).FullName, 
                typeof(uint).FullName, 
                typeof(long).FullName,
                typeof(ulong).FullName, 
                typeof(IntPtr).FullName, 
                typeof(UIntPtr).FullName, 
                typeof(char).FullName, 
                typeof(double).FullName, 
                typeof(float).FullName
            };

        /// <summary>
        /// Gets information about the types defined in the specified project.
        /// </summary>
        /// <param name="solution">The solution that contains the project.</param>
        /// <param name="projectName">Name of the project.</param>
        /// <returns>
        /// The information about all types defined in the specified project.
        /// </returns>
        public static Assembly Analyze(Solution solution, string projectName)
        {
            var project = solution.GetAllProjects().FirstOrDefault(p => p.Name.Equals(projectName, StringComparison.InvariantCultureIgnoreCase));
            if (project == null)
            {
                throw new ApplicationException(string.Format("The project '{0}' could not be found.", projectName));
            }

            var macroResolver = new MacroResolver(project);
            var targetFileName = macroResolver.Resolve("TargetFileName");
            var targetPath = macroResolver.Resolve("TargetPath");

            if (string.IsNullOrWhiteSpace(targetFileName))
            {
                throw new ApplicationException(string.Format("The project '{0}' does not have a property 'TargetNameSpace' dpecified.", projectName));
            }

            var name = Path.GetFileNameWithoutExtension(targetFileName);

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ApplicationException(string.Format("The output assembly name '{1}' of project '{0}' is invalid.", projectName, name));
            }

            var assemblyFullName = name; // TODO: get full name

            return new Assembly
            {
                Name = name,
                FullName = assemblyFullName,
                Version = "1.0.0.0", // TODO: get version
                Path = targetPath,
                Types = (from projectItem in project.ProjectItems.Cast<ProjectItem>().Iterate(pi => pi.ProjectItems.Cast<ProjectItem>())
                         where projectItem.FileCodeModel != null && projectItem.FileCodeModel.CodeElements != null
                         from nameSpace in projectItem.FileCodeModel.CodeElements.OfType<CodeNamespace>()
                         from type in nameSpace.Members.OfType<CodeType>().Iterate(t => t.Members.OfType<CodeType>())
                         select AnalyzeType(type, assemblyFullName)).ToArray()
            };
        }

        /// <summary>
        /// Analyzes the specified type and its members and attribtues.
        /// </summary>
        /// <param name="type">
        /// The Code DOM type.
        /// </param>
        /// <param name="assemblyFullName">
        /// Full name of the assembly that contains the type.
        /// </param>
        /// <param name="codeTypeRef">
        /// An optional code type reference.
        /// </param>
        /// <returns>
        /// The <see cref="Type"/> that describes the specified type.
        /// </returns>
        private static Type AnalyzeType(CodeType type, string assemblyFullName, CodeTypeRef2 codeTypeRef = null)
        {
            var codeClass = type as CodeClass2;
            var codeEnum = type as CodeEnum;
            var codeInterface = type as CodeInterface2;
            var codeStruct = type as CodeStruct2;

            var isPrimitive = PrimitiveTypes.Any(t => t == type.FullName);
            var isValueType = codeEnum != null || codeStruct != null || isPrimitive;
            var isPublic = type.Access == vsCMAccess.vsCMAccessPublic;
            var isPrivate = type.Access == vsCMAccess.vsCMAccessPrivate;
            bool isNested;
            try
            {
                isNested = type.Parent is CodeType;
            }
            catch
            {
                isNested = false;
            }
            Guid guid;
            var x = type.Attributes.OfType<CodeAttribute2>().Where(a => a.FullName == typeof(GuidAttribute).FullName).Select(a => a.Value).FirstOrDefault();
            Guid.TryParse(x, out guid);

            return new Type
            {
                Name = type.Name,
                FullName = type.FullName,
                AssemblyQualifiedName = type.FullName + ", " + assemblyFullName,
                Properties = type.Members.OfType<CodeProperty>().Select(AnalyzeProperty).ToArray(),
                Methods = type.Members.OfType<CodeFunction>().Where(m => m.FunctionKind != vsCMFunction.vsCMFunctionPropertySet && m.FunctionKind != vsCMFunction.vsCMFunctionPropertyGet).Select(AnalyzeMethod).ToArray(),
                //Attributes = type.Attributes.OfType<CodeAttribute>().Select(o => o.GetType().FullName).ToArray(),
                IsAbstract = codeClass != null && codeClass.IsAbstract,
                IsArray = codeTypeRef != null && codeTypeRef.TypeKind == vsCMTypeRef.vsCMTypeRefArray,
                IsByRef = !isValueType,
                IsComObject = false,
                IsClass = codeClass == null,
                IsEnum = codeEnum != null,
                IsExplicitLayout = type.Members.OfType<CodeVariable2>().Any(v => v.Attributes.OfType<CodeAttribute>().Any(a => a.FullName == typeof(FieldOffsetAttribute).FullName)),
                IsGenericParameter = false,
                IsGenericType = codeClass != null && codeClass.IsGeneric,
                IsGenericTypeDefinition = false,
                IsInterface = codeInterface != null,
                IsMarshalByRef = type.Bases.OfType<CodeType>().Iterate(t => t.Bases.OfType<CodeType>()).Any(t => t.FullName == typeof(MarshalByRefObject).FullName),
                IsNested = isNested,
                IsNestedAssembly = isNested && type.Access == vsCMAccess.vsCMAccessProject,
                IsNestedFamAndAssem = isNested && type.Access == vsCMAccess.vsCMAccessProject,
                IsNestedFamOrAssem = isNested && type.Access == vsCMAccess.vsCMAccessAssemblyOrFamily,
                IsNestedFamily = isNested,
                IsNestedPrivate = isNested && isPrivate,
                IsNestedPublic = isNested && isPublic,
                IsNotPublic = !isPublic,
                IsPointer = codeTypeRef != null && codeTypeRef.TypeKind == vsCMTypeRef.vsCMTypeRefPointer,
                IsPrimitive = isPrimitive,
                IsPublic = isPublic,
                IsSealed = codeClass != null && codeClass.InheritanceKind == vsCMInheritanceKind.vsCMInheritanceKindSealed,
                IsSecurityCritical = false,
                IsSecuritySafeCritical = false,
                IsSecurityTransparent = false,
                IsSerializable = codeClass != null && codeClass.Attributes.OfType<CodeAttribute2>().Any(a => a.FullName == typeof(SerializableAttribute).FullName),
                IsSpecialName = false,
                IsValueType = isValueType,
                IsVisible = type.Access == vsCMAccess.vsCMAccessPublic || type.Access == vsCMAccess.vsCMAccessProtected,
                BaseType = type.Bases == null ? null : type.Bases.Count == 0 ? null : AnalyzeType((CodeType)type.Bases.Item(1), assemblyFullName),
                Guid = guid,
                ////IsAnsiClass = type.IsAnsiClass,
                ////IsAutoClass = type.IsAutoClass,
                ////IsAutoLayout = type.IsAutoLayout,
                ////IsConstructedGenericType = type.IsConstructedGenericType,
                ////IsContextful = type.IsContextful,
                ////IsImport = type.IsImport,
                ////IsLayoutSequential = type.IsLayoutSequential,
                ////IsUnicodeClass = type.IsUnicodeClass,
            };
        }

        /// <summary>
        /// Analyzes the specified method.
        /// </summary>
        /// <param name="method">
        /// The Code DOM method.
        /// </param>
        /// <returns>
        /// The <see cref="Method"/> that describes the method.
        /// </returns>
        private static Method AnalyzeMethod(CodeFunction method)
        {
            return new Method
            {
                Name = method.Name,
                ReturnType = method.Type.AsFullName,
                //Attributes = method.Attributes.OfType<CodeAttribute>().Select(o => o.FullName).ToArray(),
                Parameters = method.Parameters.OfType<CodeParameter>().Select(AnalyzeParameter).ToArray()
            };
        }

        /// <summary>
        /// Analyzes the specified method parameter.
        /// </summary>
        /// <param name="parameter">
        /// The Code DOM parameter.
        /// </param>
        /// <returns>
        /// The <see cref="Parameter"/> that describes the parameter.
        /// </returns>
        private static Parameter AnalyzeParameter(CodeParameter parameter)
        {
            return new Parameter
            {
                Name = parameter.Name,
                Type = parameter.Type.AsFullName,
                Attributes = parameter.Attributes.OfType<CodeAttribute>().Select(o => o.FullName).ToArray(),
            };
        }

        /// <summary>
        /// Analyzes the specified property.
        /// </summary>
        /// <param name="property">
        /// The Code DOM property.
        /// </param>
        /// <returns>
        /// The <see cref="Property"/> that describes the property.
        /// </returns>
        private static Property AnalyzeProperty(CodeProperty property)
        {
            return new Property
            {
                Name = property.Name,
                CanRead = property.Getter != null && property.Getter.Access == vsCMAccess.vsCMAccessPublic,
                CanWrite = property.Setter != null && property.Setter.Access == vsCMAccess.vsCMAccessPublic,
                Type = AnalyzeType(property.Type.CodeType, string.Empty),
                Attributes = property.Attributes.OfType<CodeAttribute>().Select(o => o.FullName).ToArray(),
            };
        }
    }
}