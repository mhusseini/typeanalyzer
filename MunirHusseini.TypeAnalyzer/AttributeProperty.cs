﻿using System;

namespace MunirHusseini.TypeAnalyzer
{
    [Serializable]
    public class AttributeProperty
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}