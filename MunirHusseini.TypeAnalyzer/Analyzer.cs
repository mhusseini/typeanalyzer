﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Analyzer.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    using global::EnvDTE;

    using Microsoft.VisualStudio.TextTemplating;

    /// <summary>
    /// This class provides access to the assembly and project analysis.
    /// </summary>
    public static class Analyzer
    {
        /// <summary>
        /// Gets information about the types defined in the specified assembly.
        /// </summary>
        /// <param name="path">The path to an assembly.</param>
        /// <param name="configurationFile">An optional application configuration file.</param>
        /// <returns>The information about all types defined in the specified assembly.</returns>
        public static Assembly Analyze(string path, string configurationFile = null)
        {
            try
            {
                var dir = Path.GetDirectoryName(path);

                var appDomain = AppDomain.CreateDomain(
                    Guid.NewGuid().ToString(),
                    null,
                    new AppDomainSetup
                    {
                        ApplicationBase = dir,
                        PrivateBinPath = dir,
                        ShadowCopyFiles = "true",
                        LoaderOptimization = LoaderOptimization.MultiDomainHost,
                        ConfigurationFile = configurationFile
                    });

                var type = typeof(AssemblyAnalyzer);
                var analyzer = (IAssemblyAnalyzer)appDomain.CreateInstanceFromAndUnwrap(type.Assembly.Location, type.FullName);
                var result = analyzer.Analyze(path);

                AppDomain.Unload(appDomain);

                return result;
            }
            catch (ReflectionTypeLoadException ex)
            {
                throw new ApplicationException(string.Join("\r\n", ex.LoaderExceptions.Select(x => x.Message + "\r\n" + x.StackTrace + "\r\n-------------------------\r\n")));
            }
        }

        /// <summary>
        /// Gets information about the types defined in the specified project.
        /// </summary>
        /// <param name="projectName">Name of the project.</param>
        /// <param name="host">The templating engine host.</param>
        /// <returns>The information about all types defined in the specified project.</returns>
        public static Assembly Analyze(string projectName, ITextTemplatingEngineHost host)
        {
            // ReSharper disable SuspiciousTypeConversion.Global
            if (!(host is IServiceProvider))
            {
                throw new ArgumentException("The templating engine host does not implement IServiceProvider.");
            }

            return Analyze(projectName, (IServiceProvider)host);
            // ReSharper restore SuspiciousTypeConversion.Global
        }

        /// <summary>
        /// Analyzes the specified project name.
        /// </summary>
        /// <param name="projectName">Name of the project.</param>
        /// <param name="serviceProvider">The service provider.</param>
        /// <returns>The information about all types defined in the specified project.</returns>
        /// <exception cref="System.ArgumentException">The specified service provider did not return an instance of DTE.</exception>
        public static Assembly Analyze(string projectName, IServiceProvider serviceProvider)
        {
            var dte = serviceProvider.GetService(typeof(DTE)) as DTE;
            if (dte == null)
            {
                throw new ArgumentException("The specified service provider did not return an instance of DTE.", "serviceProvider");
            }

            var solution = dte.Solution;
            return Analyze(projectName, solution);
        }

        /// <summary>
        /// Analyzes the specified project name.
        /// </summary>
        /// <param name="projectName">Name of the project.</param>
        /// <param name="solution">The solution.</param>
        /// <returns>The information about all types defined in the specified project.</returns>
        public static Assembly Analyze(string projectName, Solution solution)
        {
            return ProjectAnalyzer.Analyze(solution, projectName);
        }
    }
}
