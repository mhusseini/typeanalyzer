﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace MunirHusseini.TypeAnalyzer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args == null || !args.Any())
            {
                Console.Error.WriteLine("No assembly specified.");
                Environment.Exit(1);
            }

            var path = args.FirstOrDefault();

            try
            {
                var analyzer = new AssemblyAnalyzer();
                var result = analyzer.Analyze(path);

                var serializer = JsonSerializer.Create();
                var sb = new StringBuilder();
                using (var w = new StringWriter(sb))
                {
                    serializer.Serialize(w, result);
                }

                Console.WriteLine(sb.ToString());
                Environment.Exit(0);
            }
            catch (ReflectionTypeLoadException ex)
            {
                foreach (var ex2 in ex.LoaderExceptions)
                {
                    Console.Error.WriteLine(ex2.Message);
                    Console.Error.WriteLine(ex2.StackTrace);
                    var fnf = ex2 as FileNotFoundException;
                    if (fnf != null)
                    {
                        Console.Error.WriteLine("-------- FUSION LOG:");
                        Console.Error.WriteLine(fnf.FusionLog);
                    }
                    Console.Error.WriteLine("----------------");
                }
            }
            catch (Exception ex)
            {
                while (ex != null)
                {
                    Console.Error.WriteLine(ex.Message);
                    Console.Error.WriteLine(ex.StackTrace);
                    Console.Error.WriteLine("----------------");
                    ex = ex.InnerException;
                }
            }
        }
    }
}