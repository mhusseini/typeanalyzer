using System;

namespace MunirHusseini.TypeAnalyzer
{
    [Serializable]
    public class Attribute
    {
        public string FullName { get; set; }
        public AttributeProperty[] Properties { get; set; }
    }
}