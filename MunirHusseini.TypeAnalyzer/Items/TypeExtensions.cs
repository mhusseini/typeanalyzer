﻿using System.Collections.Generic;

namespace MunirHusseini.TypeAnalyzer
{
    public static class TypeExtensions
    {
        public static IEnumerable<Type> GetBaseTypes(this Type type)
        {
            type = type.BaseType;
            while (type != null)
            {
                yield return type;
                type = type.BaseType;
            }
        }
    }
}