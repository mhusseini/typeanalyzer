﻿using System;

namespace MunirHusseini.TypeAnalyzer
{
    [Serializable]
    public class EnumerableInfo
    {
        public EnumerableInfo NestedEnumerable { get; set; }

        internal string EnumerableTypeName { get; set; }

        [NonSerialized]
        private Type _enumerableType;

        public Type EnumerableType
        {
            get { return _enumerableType; }
            set
            {
                _enumerableType = value;
                EnumerableTypeName = value == null ? null : value.FullName ?? Guid.NewGuid().ToString();
            }
        }

        internal string ItemTypeName { get; set; }

        [NonSerialized]
        private Type _itemType;

        public Type ItemType
        {
            get { return _itemType; }
            set
            {
                _itemType = value;
                ItemTypeName = value == null ? null : value.FullName ?? Guid.NewGuid().ToString();
            }
        }
    }
}