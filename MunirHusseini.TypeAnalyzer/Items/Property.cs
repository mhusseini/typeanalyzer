﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Property.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
    using System;

    /// <summary>
    /// Describes a types property.
    /// </summary>
    [Serializable]
    public class Property
    {
        [NonSerialized]
        private Type _type;

        /// <summary>
        /// Gets or sets the property's attributes.
        /// </summary>
        public string[] Attributes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the property can be read.
        /// </summary>
        public bool CanRead { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the property can be set.
        /// </summary>
        public bool CanWrite { get; set; }

        /// <summary>
        /// Gets or sets the name of the type that declared the property.
        /// </summary>
        public string DeclaringType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is abstract.
        /// </summary>
        public bool IsAbstract { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is declared internal..
        /// </summary>
        public bool IsInternal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is not declared public.
        /// </summary>
        public bool IsNotPublic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is declared private.
        /// </summary>
        public bool IsPrivate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is declared protected.
        /// </summary>
        public bool IsProtected { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is declared public.
        /// </summary>
        public bool IsPublic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is declared static.
        /// </summary>
        public bool IsStatic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is a virtual member.
        /// </summary>
        public bool IsVirtual { get; set; }

        /// <summary>
        /// Gets or sets the property name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the property type.
        /// </summary>
        public Type Type
        {
            get { return _type; }
            set
            {
                _type = value;
                TypeName = value == null ? null : value.FullName ?? Guid.NewGuid().ToString();
            }
        }

        internal string TypeName { get; set; }
    }
}