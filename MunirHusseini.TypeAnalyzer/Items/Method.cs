﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Method.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
    using System;
    using System.Linq;

    /// <summary>
    /// Describes a types method.
    /// </summary>
    [Serializable]
    public class Method
    {
        /// <summary>
        /// Gets or sets the methods attributes.
        /// </summary>
        public Attribute[] Attributes { get; set; }

        public string DeclaringType { get; set; }

        public bool IsAbstract { get; set; }

        public bool IsFamily { get; set; }

        public bool IsFamilyAndAssembly { get; set; }

        public bool IsFamilyOrAssembly { get; set; }

        public bool IsFinal { get; set; }

        public bool IsPrivate { get; set; }

        public bool IsPublic { get; set; }

        public bool IsStatic { get; set; }

        public bool IsVirtual { get; set; }

        /// <summary>
        /// Gets or sets the methods name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the methods parameters.
        /// </summary>
        public Parameter[] Parameters { get; set; }

        /// <summary>
        /// Gets or sets the methodsd return type.
        /// </summary>
        public string ReturnType { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var n = this.Name ?? string.Empty;
            var p = this.Parameters ?? new Parameter[0];

            return $"{n}({string.Join(", ", p.Select(p2 => p2.Type))})";
        }
    }
}