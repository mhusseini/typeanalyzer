﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Assembly.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

    /// <summary>
    /// Describes an assembly and the types defined in it.
    /// </summary>
    [Serializable]
    public class Assembly
    {
        /// <summary>
        /// Gets or sets the assembly name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the assembly full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the assembly version.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets the assembly path.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the types defined in the assembly.
        /// </summary>
        public Type[] Types { get; set; }
        internal Type[] ExternalTypes { get; set; }

        [OnSerializing]
        internal void OnSerializing(StreamingContext context)
        {
            var internalTypes = new HashSet<string>(Types.Select(t => t.FullName));
            var externalTypes = new Dictionary<string, Type>();

            foreach (var type in Types)
            {
                var ei = type.EnumerableInfo;

                while (ei != null)
                {
                    if (!internalTypes.Contains(ei.EnumerableTypeName) && !externalTypes.ContainsKey(ei.EnumerableTypeName))
                    {
                        externalTypes.Add(ei.EnumerableTypeName, ei.EnumerableType);
                    }

                    if (!internalTypes.Contains(ei.ItemTypeName) && !externalTypes.ContainsKey(ei.ItemTypeName))
                    {
                        externalTypes.Add(ei.ItemTypeName, ei.ItemType);
                    }

                    ei = ei.NestedEnumerable;
                }

                foreach (var property in type.Properties.Where(p => !internalTypes.Contains(p.TypeName) && !externalTypes.ContainsKey(p.TypeName)))
                {
                    externalTypes.Add(property.TypeName, property.Type);
                }
            }

            ExternalTypes = externalTypes.Values.ToArray();
        }

        [OnDeserialized]
        internal void OnDeserialized(StreamingContext context)
        {
            var allTypes = ExternalTypes.Union(Types).ToList();
            var types = allTypes.ToDictionary(t => t.FullName, t => t);

            foreach (var type in allTypes)
            {
                var ei = type.EnumerableInfo;

                while (ei != null)
                {
                    ei.EnumerableType = GetType(types, ei.EnumerableTypeName);
                    ei.ItemType = GetType(types, ei.ItemTypeName);

                    ei = ei.NestedEnumerable;
                }

                foreach (var property in type.Properties)
                {
                    property.Type = GetType(types, property.TypeName);
                }
            }
        }

        private static Type GetType(IReadOnlyDictionary<string, Type> types, string name)
        {
            Type t;

            if (types.TryGetValue(name, out t))
            {
                return t;
            }

            var t2 = System.Type.GetType(name, false);
            if (t2 != null)
            {
                t = AssemblyAnalyzer.AnalyzeType(t2);
            }
            
            return t;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return FullName;
        }
    }
}