using System;

namespace MunirHusseini.TypeAnalyzer
{
    /// <summary>
    /// Represents a value of an enum.
    /// </summary>
    [Serializable]
    public class EnumValue
    {
        /// <summary>
        /// The textual represetation of the enum value.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The numerical value of the enum value.
        /// </summary>
        public int? Value { get; set; }
    }
}