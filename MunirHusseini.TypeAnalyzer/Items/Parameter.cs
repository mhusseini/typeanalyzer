﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Parameter.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
    using System;

    /// <summary>
    /// Describes a method parameter,
    /// </summary>
    [Serializable]
    public class Parameter
    {
        /// <summary>
        /// Gets or sets the parameters name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the parameters type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the parameters attributes.
        /// </summary>
        public string[] Attributes { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }
    }
}