﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Type.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
    using System;

    /// <summary>
    /// Describes a certain Type.
    /// </summary>
    [Serializable]
    public class Type
    {
        /// <summary>
        /// Gets or sets the type assembly qualified name.
        /// </summary>
        public string AssemblyQualifiedName { get; set; }

        /// <summary>
        /// Gets or sets the attributes.
        /// </summary>
        public Attribute[] Attributes { get; set; }

        /// <summary>
        /// Gets or sets the base type.
        /// </summary>
        public Type BaseType { get; set; }

        /// <summary>
        /// Gets or sets the full name of the type as it would be written in code.
        /// </summary>
        public string CodeFullName { get; set; }

        /// <summary>
        /// Gets or sets the name of the type as it would be written in code.
        /// </summary>
        public string CodeName { get; set; }

        public EnumerableInfo EnumerableInfo { get; set; }

        /// <summary>
        /// The enum values of the type.
        /// </summary>
        /// <remarks>This value is only valid the the <see cref="P:IsEnum"/> property is <c>true</c>;</remarks>
        public EnumValue[] EnumValues { get; set; }

        /// <summary>
        /// Gets or sets the type full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the type GUID.
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is abstract.
        /// </summary>
        public bool IsAbstract { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is an ansi class.
        /// </summary>
        public bool IsAnsiClass { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is array.
        /// </summary>
        public bool IsArray { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the string format attribute AutoClass is selected for the Type.
        /// </summary>
        public bool IsAutoClass { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the fields of the current type are laid out automatically by the common language runtime.
        /// </summary>
        public bool IsAutoLayout { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is by ref.
        /// </summary>
        public bool IsByRef { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is passed by reference.
        /// </summary>
        public bool IsClass { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is a COM object.
        /// </summary>
        public bool IsComObject { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this object represents a constructed generic type. You can create instances of a constructed generic type.
        /// </summary>
        public bool IsConstructedGenericType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Type can be hosted in a context.
        /// </summary>
        public bool IsContextful { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is enum.
        /// </summary>
        public bool IsEnum { get; set; }

        public bool IsEnumerable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the fields of the current type are laid out at explicitly specified offsets.
        /// </summary>
        public bool IsExplicitLayout { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current Type represents a type parameter in the definition of a generic type or method.
        /// </summary>
        public bool IsGenericParameter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current type is a generic type.
        /// </summary>
        public bool IsGenericType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current type represents a generic type definition, from which other generic types can be constructed.
        /// </summary>
        public bool IsGenericTypeDefinition { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type has a ComImportAttribute attribute applied, indicating that it was imported from a COM type library.
        /// </summary>
        public bool IsImport { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is an interface; that is, not a class or a value type.
        /// </summary>
        public bool IsInterface { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the fields of the current type are laid out sequentially, in the order that they were defined or emitted to the metadata.
        /// </summary>
        public bool IsLayoutSequential { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is marshaled by reference.
        /// </summary>
        public bool IsMarshalByRef { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type object represents a type whose definition is nested inside the definition of another type.
        /// </summary>
        public bool IsNested { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is nested and visible only within its own assembly.
        /// </summary>
        public bool IsNestedAssembly { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is nested and visible only to classes that belong to both its own family and its own assembly.
        /// </summary>
        public bool IsNestedFamAndAssem { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is nested and visible only within its own family.
        /// </summary>
        public bool IsNestedFamily { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is nested and visible only to classes that belong to either its own family or to its own assembly.
        /// </summary>
        public bool IsNestedFamOrAssem { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is nested and declared private.
        /// </summary>
        public bool IsNestedPrivate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is nested and declared public.
        /// </summary>
        public bool IsNestedPublic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is not declared public.
        /// </summary>
        public bool IsNotPublic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is a pointer.
        /// </summary>
        public bool IsPointer { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is a primitive type.
        /// </summary>
        public bool IsPrimitive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is declared public..
        /// </summary>
        public bool IsPublic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is declared sealed.
        /// </summary>
        public bool IsSealed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is security-critical or security-safe-critical at the current trust level, and therefore can perform critical operations.
        /// </summary>
        public bool IsSecurityCritical { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is security-safe-critical at the current trust level; that is, whether it can perform critical operations and can be accessed by transparent code.
        /// </summary>
        public bool IsSecuritySafeCritical { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is transparent at the current trust level, and therefore cannot perform critical operations.
        /// </summary>
        public bool IsSecurityTransparent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is serializable.
        /// </summary>
        public bool IsSerializable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type has a name that requires special handling.
        /// </summary>
        public bool IsSpecialName { get; set; }

        public bool IsSystem { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the string format attribute UnicodeClass is selected for the Type.
        /// </summary>
        public bool IsUnicodeClass { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type is a value type.
        /// </summary>
        public bool IsValueType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the type can be accessed by code outside the assembly.
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Gets or sets the methods.
        /// </summary>
        public Method[] Methods { get; set; }

        /// <summary>
        /// Gets or sets the type name.
        /// </summary>
        public string Name { get; set; }

        public string Namespace { get; set; }

        /// <summary>
        /// Gets or sets the properties.
        /// </summary>
        public Property[] Properties { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return CodeFullName;
        }
    }
}