﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines some usefull convenience methods.
    /// </summary>
    internal static class Extensions
    {
        /// <summary>
        /// Iterates the specified item and subsequent items as an <see cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/>.
        /// </summary>
        /// <typeparam name="T">The type of the object to iterate.</typeparam>
        /// <param name="item">The starting item.</param>
        /// <param name="next">A delegate that gets the current item as input and returns the next item.</param>
        /// <returns>The root item and all items returned by the "next" delegate as an IEnmuerable.</returns>
        public static IEnumerable<T> Iterate<T>(this T item, Func<T, T> next) where T : class
        {
            while (item != null)
            {
                yield return item;
                item = next(item);
            }
        }

        /// <summary>
        /// Iterates the specified item and subsequent items as an <see cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/>.
        /// </summary>
        /// <typeparam name="T">The type of the object to iterate.</typeparam>
        /// <param name="item">The starting item.</param>
        /// <param name="next">A delegate that gets the current item as input and returns the next items.</param>
        /// <returns>The root item and all items returned by the "next" delegate as an IEnmuerable.</returns>
        public static IEnumerable<T> Iterate<T>(this T item, Func<T, IEnumerable<T>> next) where T : class
        {
            if (item == null)
            {
                yield break;
            }

            yield return item;

            foreach (var child2 in next(item).SelectMany(child => Iterate(child, next)))
            {
                yield return child2;
            }
        }

        /// <summary>
        /// Iterates the specified items and subsequent items as an <see cref="System.Collections.Generic.IEnumerable&lt;T&gt;"/>.
        /// </summary>
        /// <typeparam name="T">The type of the object to iterate.</typeparam>
        /// <param name="items">The starting item.</param>
        /// <param name="next">A delegate that gets the current item as input and returns the next items.</param>
        /// <returns>The root items and all items returned by the "next" delegate as an IEnmuerable.</returns>
        public static IEnumerable<T> Iterate<T>(this IEnumerable<T> items, Func<T, IEnumerable<T>> next) where T : class
        {
            return items.SelectMany(child => Iterate(child, next));
        }

        public static object NotNull(this object obj, Func<object, object> value)
        {
            return obj == null ? null : value(obj);
        }
    }
}