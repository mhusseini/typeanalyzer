﻿using System;
using System.Runtime.Serialization;

namespace MunirHusseini.TypeAnalyzer
{
    [Serializable]
    public class AssemblyAnalysisFailedException : Exception
    {
        protected AssemblyAnalysisFailedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        public AssemblyAnalysisFailedException(System.Type type, Exception innerException) : base(
            $"Could not analyze the type '{type.FullName}'", innerException)
        {
        }
    }
}