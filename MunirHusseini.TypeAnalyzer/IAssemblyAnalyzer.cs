﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IAssemblyAnalyzer.cs" company="Munir Husseini">
// (c) 2014 Munir Husseini
// </copyright>
// <summary>
//   Contains the Analyzer Class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MunirHusseini.TypeAnalyzer
{
    /// <summary>
    /// Defines the methods for assembly analyzers.
    /// </summary>
    internal interface IAssemblyAnalyzer
    {
        /// <summary>
        /// Gets information about the types defined in the specified project.
        /// </summary>
        /// <param name="path">The path to the assembly to analyze.</param>
        /// <returns>
        /// The information about all types defined in the specified project.
        /// </returns>
        Assembly Analyze(string path);
    }
}