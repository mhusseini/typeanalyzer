﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MunirHusseini.TypeAnalyzer;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //var ass = Analyzer.Analyze(@"C:\tmp\WebClientDemo\Dcx.Plus.WebClient.Demo\bin\Dcx.Plus.Business.Common.dll");
            var ass = Analyzer.Analyze(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var methods = ass.Types.First(t => t.Name == "TestClass").Methods;
        }
    }

    public class TestClass
    {
        public int Property1 { get; set; }

        public void Method1()
        {
        }
    }
}
