﻿using System;
using System.Text.RegularExpressions;
using EnvDTE;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio.TextTemplating;

namespace MunirHusseini.EnvDTE
{
    public class MacroResolver
    {
        private IVsBuildMacroInfo macroInfo;

        public MacroResolver(object template)
        {
            object o;
            try
            {
                o = ((dynamic)template).Host;
            }
            catch (RuntimeBinderException)
            {
                throw new ArgumentException("The specified object does not contain a property named 'Host'.", nameof(template));
            }

            var host = o as ITextTemplatingEngineHost;

            if (host == null)
            {
                throw new ArgumentException("The specified object does not contain a property named 'Host' of type Microsoft.VisualStudio.TextTemplating.ITextTemplatingEngineHost.", nameof(template));
            }

            this.Init(host);
        }

        public MacroResolver(ITextTemplatingEngineHost host)
        {
            this.Init(host);
        }

        public MacroResolver(Project project)
        {
            var serviceProvider = new ServiceProvider((Microsoft.VisualStudio.OLE.Interop.IServiceProvider)project.DTE);
            this.Init(project, serviceProvider);
        }

        public MacroResolver(IVsHierarchy hierarchy)
        {
            this.Init(hierarchy);
        }

        public string Resolve(string macro)
        {
            string result;
            this.macroInfo.GetBuildMacroValue(macro, out result);
            return result;
        }

        private static readonly Regex RegexMacro = new Regex(@"\$\((?<macro>\w+)\)", RegexOptions.Compiled);
        public string Parse(string text)
        {
            var result = RegexMacro.Replace(text, m => this.Resolve(m.Groups[1].Value));
            return result;
        }

        private void Init(ITextTemplatingEngineHost host)
        {
            var serviceProvider = (IServiceProvider)host;
            var dte = (DTE)serviceProvider.GetService(typeof(DTE));
            var projectItem = dte.Solution.FindProjectItem(host.TemplateFile);
            var project = projectItem.ContainingProject;

            this.Init(project, serviceProvider);
        }

        private void Init(Project project, IServiceProvider serviceProvider)
        {
            var solution = (IVsSolution)serviceProvider.GetService(typeof(SVsSolution));
            IVsHierarchy hierarchy;
            solution.GetProjectOfUniqueName(project.FullName, out hierarchy);

            this.Init(hierarchy);
        }

        private void Init(IVsHierarchy hierarchy)
        {
            this.macroInfo = (IVsBuildMacroInfo)hierarchy;
        }
    }
}