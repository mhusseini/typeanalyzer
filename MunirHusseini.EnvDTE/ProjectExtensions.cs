﻿using System;
using EnvDTE;

namespace MunirHusseini.EnvDTE
{
    public static class ProjectExtensions
    {
        public static string GetDefaultNamespace(this Project project)
        {
            if(project == null) throw new ArgumentNullException("project");

            var prop = project.Properties.Item("DefaultNamespace");
            if(prop == null || prop.Value == null) throw new ApplicationException("Cannot retrieve default namespace from project '" + project.Name + "'.");

            return prop.Value.ToString();
        }
    }
}